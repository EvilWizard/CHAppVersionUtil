# CHAppVersionUtil

[![CI Status](https://img.shields.io/travis/EvilWizard/CHAppVersionUtil.svg?style=flat)](https://travis-ci.org/EvilWizard/CHAppVersionUtil)
[![Version](https://img.shields.io/cocoapods/v/CHAppVersionUtil.svg?style=flat)](https://cocoapods.org/pods/CHAppVersionUtil)
[![License](https://img.shields.io/cocoapods/l/CHAppVersionUtil.svg?style=flat)](https://cocoapods.org/pods/CHAppVersionUtil)
[![Platform](https://img.shields.io/cocoapods/p/CHAppVersionUtil.svg?style=flat)](https://cocoapods.org/pods/CHAppVersionUtil)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CHAppVersionUtil is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'CHAppVersionUtil'
```

## Author

EvilWizard, duanchao19900812@gmail.com

## License

CHAppVersionUtil is available under the MIT license. See the LICENSE file for more info.
