Pod::Spec.new do |s|
  s.name             = 'CHAppVersionUtil'
  s.version          = '0.1.0'
  s.summary          = 'iOS App伪强制更新.'
  s.description      = <<-DESC
  iOS App伪强制更新.可搭配后台接口按需强制更新
                       DESC
  s.homepage         = 'https://gitlab.com/EvilWizard/CHAppVersionUtil.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'EvilWizard' => 'duanchao19900812@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/EvilWizard/CHAppVersionUtil.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  s.source_files = 'CHAppVersionUtil/Classes/**/*'
  s.dependency 'AFNetworking', '~> 3.2.0'
end
