//
//  AppVersionUtil.h
//  MenDianBao
//
//  Created by 行者栖处 on 2018/4/2.
//  Copyright © 2018年 apple. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppVersionUtil : NSObject

/**
 初始化
 在
 application:didFinishLaunchingWithOptions:中rootViewController设置完之后
 applicationWillEnterForeground:中调用
 @param appID itunes显示的AppID唯一标识
 @return 实例
 */
+ (instancetype)openWithAppID:(NSString *)appID;


@end
