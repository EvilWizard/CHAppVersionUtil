//
//  AppVersionUtil.m
//  MenDianBao
//
//  Created by 行者栖处 on 2018/4/2.
//  Copyright © 2018年 apple. All rights reserved.
//

#import "AppVersionUtil.h"
#import "AFNetworking.h"

@interface AppVersionUtil ()

/**
 *  当前版本
 */
@property (nonatomic, copy) NSString *currentVersion;

/**
 *  最新版本
 */
@property (nonatomic, copy) NSString *lastestVersion;

@end

NSString *const kItunesStoreURI = @"http://itunes.apple.com/cn/lookup?id=";
NSString *const kAppVersionReleaseNotes = @"releaseNotes";
NSString *const kAppVersionLastest = @"version";
NSString *const kAppVersionLastestURL = @"trackViewUrl";

@implementation AppVersionUtil

+ (instancetype)openWithAppID:(NSString *)appID {
    AppVersionUtil *versionUtil = [[AppVersionUtil alloc] init];
    [versionUtil queryLatestAppVersionWith:appID];
    return versionUtil;
}

- (void)queryLatestAppVersionWith:(NSString *)appID {
    if (!appID ||
        ![appID isKindOfClass:[NSString class]] ||
        [appID isEqualToString:@""] ||
        [appID isEqual:[NSNull null]]) {
        return;
    }
    NSString *url = [NSString stringWithFormat:@"%@%@",kItunesStoreURI,appID];
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:url parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if ([responseObject isKindOfClass:[NSDictionary class]]) {
            NSArray *results = responseObject[@"results"];
            NSDictionary * result = results.firstObject;
            if ([result isKindOfClass:[NSDictionary class]]) {
                [self showAlertWithLatestVersion:result];
            }
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
}

- (void)showAlertWithLatestVersion:(NSDictionary *)itunesAppInfos {
    if (![itunesAppInfos isKindOfClass:[NSDictionary class]] ||
        itunesAppInfos.count <= 0) {
        return;
    }
    NSString *lastestVersion = itunesAppInfos[kAppVersionLastest];
    if ([self isLatestVersionWith:lastestVersion]) {
        return;
    }
    NSString *title = [NSString stringWithFormat:@"版本更新%@",lastestVersion];
    NSString *message = [NSString stringWithFormat:@"\n%@",itunesAppInfos[kAppVersionReleaseNotes]];
    UIAlertController *alertVc = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    NSString *path = itunesAppInfos[kAppVersionLastestURL];
    NSURL *url = [NSURL URLWithString:path];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"去更新" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[UIApplication sharedApplication] openURL:url ];
    }];
    [alertVc addAction:confirmAction];
    [[UIApplication sharedApplication].delegate.window.rootViewController presentViewController:alertVc animated:YES completion:^{
        
    }];
}

// 判断是不是最新版本
- (BOOL)isLatestVersionWith:(NSString *)version {
    NSArray <NSString *>*currentArray = [self.currentVersion componentsSeparatedByString:@"."];
    NSArray <NSString *>*latestArray = [version componentsSeparatedByString:@"."];
    NSInteger count = MAX(currentArray.count, latestArray.count);
    for (int i = 0; i < count; i++) {
        NSInteger latest = latestArray[i].integerValue;
        NSInteger current = currentArray[i].integerValue;
        if (latest > current) {
            return NO;
        }
    }
    return YES;
}

- (NSString *)currentVersion {
    if (!_currentVersion) {
        NSString *app_version = [NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"];
        _currentVersion = app_version;
    }
    return _currentVersion;
}

@end
